package com.daven.modelviewcontroller;

import javax.swing.SwingWorker;

public class StepManager extends SwingWorker<Boolean, Object>
{
	private ModelManager board;
	
	@Override
	protected Boolean doInBackground() throws Exception
	{
		board.moveAllObjects();
		return new Boolean( true );
	}
	
	@Override
	protected void done()
	{
		board.modelChanged();
	}
	
	public StepManager( ModelManager board )
	{
		super();
		this.board = board;
	}
}
