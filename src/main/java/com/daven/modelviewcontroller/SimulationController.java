package com.daven.modelviewcontroller;

public class SimulationController extends Thread implements Runnable
{
	private ModelManager board;
	private boolean keepRunning = false;
	
	@Override
	public void run()
	{
		try
		{
			while ( true )
			{
				if ( this.keepRunning )
				{
					StepManager sm = new StepManager( board );
					sm.execute();
				}
				Thread.sleep( board.getSimulationDelay() );
			}
		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	public SimulationController( ModelManager board )
	{
		this.board = board;
	}
	
	public void startMoves()
	{
		this.keepRunning = true;
	}
	
	public void stopMoves()
	{
		this.keepRunning = false;
	}
}
