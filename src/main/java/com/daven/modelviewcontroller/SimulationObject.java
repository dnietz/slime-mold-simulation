package com.daven.modelviewcontroller;

import java.awt.Point;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public interface SimulationObject
{
	public void paint();
	
	public Point getPosition();
	
	public EightPointCompass getOrientation();
	
	public void move();
}
