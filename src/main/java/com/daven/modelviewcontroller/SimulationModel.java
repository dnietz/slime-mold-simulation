package com.daven.modelviewcontroller;

public interface SimulationModel
{
	public void addSimulationObject( SimulationObject so );
	
	public void modelChanged();
}
