package com.daven.modelviewcontroller.squaremapping;

public enum EightPointCompass
{
	NORTH,
	NORTHEAST,
	EAST,
	SOUTHEAST,
	SOUTH,
	SOUTHWEST,
	WEST,
	NORTHWEST;
	
	private EightPointCompass() {}
	
	public static EightPointCompass fromOrdinal( int ordinal )
	{
		switch( ordinal )
		{
			case -1:
				return EightPointCompass.NORTHWEST;
			case 0:
				return EightPointCompass.NORTH;
			case 1:
				return EightPointCompass.NORTHEAST;
			case 2:
				return EightPointCompass.EAST;
			case 3:
				return EightPointCompass.SOUTHEAST;
			case 4:
				return EightPointCompass.SOUTH;
			case 5:
				return EightPointCompass.SOUTHWEST;
			case 6:
				return EightPointCompass.WEST;
			case 7:
				return EightPointCompass.NORTHWEST;
			case 8: default:
				return EightPointCompass.NORTH;
		}
	}
	
	public EightPointCompass turnLeft()
	{
		return fromOrdinal( this.ordinal() - 1 );
	}
	
	public EightPointCompass turnRight()
	{
		return fromOrdinal( this.ordinal() + 1 );
	}
}
