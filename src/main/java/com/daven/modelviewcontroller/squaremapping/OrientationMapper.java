package com.daven.modelviewcontroller.squaremapping;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

public class OrientationMapper
{
	private int boardPixelsX;
	private int boardPixelsY;
	private int patchesPixelSize;
	
	public OrientationMapper( int patchesX, int patchesY, int patchesPixelSize )
	{
		this.boardPixelsX = patchesX * patchesPixelSize;
		this.boardPixelsY = patchesY * patchesPixelSize;
		this.patchesPixelSize = patchesPixelSize;
	}
	
	public Point getLocation( Point position, EightPointCompass orientation, int distance )
	{
		Point newPosition = new Point( position );
		int pixelDistance = patchesPixelSize * distance;
		switch( orientation )
		{
			case NORTH:
				newPosition.y -= pixelDistance;
				break;
			case NORTHEAST:
				newPosition.x += pixelDistance;
				newPosition.y -= pixelDistance;
				break;
			case EAST:
				newPosition.x += pixelDistance;
				break;
			case SOUTHEAST:
				newPosition.x += pixelDistance;
				newPosition.y += pixelDistance;
				break;
			case SOUTH:
				newPosition.y += pixelDistance;
				break;
			case SOUTHWEST:
				newPosition.x -= pixelDistance;
				newPosition.y += pixelDistance;
				break;
			case WEST:
				newPosition.x -= pixelDistance;
				break;
			case NORTHWEST:
				newPosition.x -= pixelDistance;
				newPosition.y -= pixelDistance;
		}
		return rollOver( newPosition );
	}
	
	public List<Point> getAllAdjoining( Point location )
	{
		List<Point> adjoiningPoints = new ArrayList<Point>(8);
		for ( EightPointCompass heading : EightPointCompass.values() )
		{
			adjoiningPoints.add( new Point( getLocation( location, heading, 1 ) ) );
		}
		return adjoiningPoints;
	}
	
	private Point rollOver( Point p )
	{
		return new Point( rollOver( p.x, boardPixelsX ), rollOver( p.y, boardPixelsY ) );
	}
	
	private int rollOver( int c, int threshold )
	{
		while ( c >= threshold )
		{
			c -= threshold;
		}
		while ( c < 0 )
		{
			c += threshold;
		}
		return c;
	}
}
