package com.daven.modelviewcontroller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JToolBar;
import javax.swing.Timer;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

@SuppressWarnings( "serial" )
public final class SimulationView extends JFrame implements ChangeListener
{
	//TODO Add annotations
	//TODO Add multithreading
	//TODO Add JUnits
	/* 
	 * Constants governing the UI and simulation display area
	 */
	private static final int FRAMEWIDTH = 700;
	private static final int FRAMEHEIGHT = 500;
	private static final int BUTTONSEPARATORWIDTH = 20;
	private static final int PATCHESCOUNTSIDE = 50;
	private static final int PATCHPIXELSIDE = 10;
	private static final int DISPLAYAREAPIXELSIDE = PATCHESCOUNTSIDE * PATCHPIXELSIDE;
	private static final int MAXDELAYMILLIS = 1000;
	
	/*
	 * Fields representing the UI's Swing components
	 */
	private static final JToolBar TOOLBAR = new JToolBar();
	private static JButton setupButton = new JButton( "Setup" );
	private static JButton stepButton = new JButton( "Step" );
	private static Timer stepButtonTimer;
	private static JButton goButton = new JButton( "Go" );
	private static JSlider initialPopulationSlider;
	private static JSlider simulationSpeedSlider;
	private static JPanel displayArea;

	/*
	 * Fields representing the elements of the Model View Controller
	 */
	private static SimulationView theView = null;
	private static ModelManager board;
	private static SimulationController runner;
	
	private SimulationView() throws Exception
	{
		super();
		if ( theView != null )
			throw new Exception( "Don't instantiate more than one of these!" );
		this.setDefaultCloseOperation( EXIT_ON_CLOSE );
	}
	
	/**
	 * Constructor that allows access to the singleton instance
	 * of the View element
	 * @return the SimulationView
	 */
	public static SimulationView getTheSimulationView()
	{
		try
		{
			if ( theView == null )
			{
				theView = setupView();
			}
		}
		catch ( Exception ex )
		{
			System.out.println( ex.getMessage() );
		}
		
		return theView;
	}
	
	private static SimulationView setupView() throws Exception
	{
		SimulationView view = new SimulationView();
		view.setMinimumSize( new Dimension( FRAMEWIDTH, FRAMEHEIGHT ) );
		view.setTitle( "Slimemold Simulator V3" );
		theView = view;
		theView.getContentPane().add( setupToolBar(), BorderLayout.WEST );
		theView.getContentPane().add( setupDisplayArea(), BorderLayout.EAST );
		theView.pack();
		board = new ModelManager();
		runner = new SimulationController( board );
		setSimulationSpeed();
		runner.start();
		theView.setVisible( true );
		return theView;
	}
	
	private static JPanel setupDisplayArea()
	{
		displayArea = new JPanel();
		displayArea.setBackground( Color.BLACK );
		Dimension panelSize = new Dimension( DISPLAYAREAPIXELSIDE, DISPLAYAREAPIXELSIDE );
		displayArea.setPreferredSize( panelSize );
		return displayArea;
	}

	private static JToolBar setupToolBar()
	{
		Dimension toolBarSize = new Dimension(200, 500);
		TOOLBAR.setPreferredSize( toolBarSize );
		TOOLBAR.setLayout( new BoxLayout( TOOLBAR, BoxLayout.Y_AXIS ) );
		TOOLBAR.add( Box.createVerticalStrut( 50 ) );
		TOOLBAR.add( setupSetupButton( setupButton ) );
		TOOLBAR.add( Box.createVerticalStrut( BUTTONSEPARATORWIDTH ) );
		TOOLBAR.add( setupStepButton( stepButton ) );
		TOOLBAR.add( Box.createVerticalStrut( BUTTONSEPARATORWIDTH ) );
		TOOLBAR.add( setupGoButton( goButton ) );
		TOOLBAR.add(  Box.createVerticalStrut( BUTTONSEPARATORWIDTH ) );
		JLabel populationSliderLabel = new JLabel( "Initial Population" );
		initializeComponent( populationSliderLabel );
		TOOLBAR.add( populationSliderLabel );
		TOOLBAR.add( setupInitialPopulationSlider() );
		TOOLBAR.add(  Box.createVerticalStrut( BUTTONSEPARATORWIDTH ) );
		JLabel speedSliderLabel = new JLabel( "Simulation Speed" );
		initializeComponent( speedSliderLabel );
		TOOLBAR.add( speedSliderLabel );
		TOOLBAR.add( setupSimulationSpeedSlider() );
		TOOLBAR.setFloatable( false );
		return TOOLBAR;
	}

	private static JButton setupSetupButton( JButton button )
	{
		initializeComponent( button );
		button.addActionListener( new ActionListener()
			{
				@Override
				public void actionPerformed( ActionEvent e )
				{
					board.runInitialSetup();
				}
			}
		);
		return button;
	}
	
	private static JButton setupStepButton( JButton button )
	{
		initializeComponent( button );
		button.addMouseListener( new MouseListener()
		{
			@Override
			public void mouseClicked( MouseEvent e ) {}

			@Override
			public void mouseEntered( MouseEvent e ) {}

			@Override
			public void mouseExited( MouseEvent e ) {}

			@Override
			public void mousePressed( MouseEvent e )
			{
				board.moveOneRound();
				Action keepMovingOneRound = new AbstractAction()
				{
					@Override
					public void actionPerformed( ActionEvent e )
					{
						board.moveOneRound();
					}
				};
				stepButtonTimer = new Timer( 400, keepMovingOneRound);
				stepButtonTimer.start();
			}

			@Override
			public void mouseReleased( MouseEvent e )
			{
				stepButtonTimer.stop();
			}
		});
		return button;
	}
	
	private static JButton setupGoButton( JButton button )
	{
		initializeComponent( button );
		button.addActionListener( new ActionListener()
		{
			@Override
			public void actionPerformed( ActionEvent e )
			{
				if ( goButton.getText().equals( "Go" ))
				{
					runner.startMoves();
					goButton.setText( "Stop" );
				}
				else if ( goButton.getText().equals( "Stop" ) )
				{
					runner.stopMoves();
					goButton.setText( "Go" );
				}
			}
		});
		return button;
	}
	
	private static void initializeComponent( JComponent component )
	{
		component.setAlignmentX( Component.CENTER_ALIGNMENT);
	}

	private static JSlider setupSimulationSpeedSlider()
	{
		JSlider slider = new JSlider();
		slider.setMinimum( 0 );
		slider.setMaximum( MAXDELAYMILLIS );
		slider.setMajorTickSpacing( MAXDELAYMILLIS / 4 );
		slider.setMinorTickSpacing( MAXDELAYMILLIS / 20 );
		slider.setPaintTicks( true );
		slider.setValue( 501 );
		simulationSpeedSlider = slider;
		simulationSpeedSlider.addChangeListener( SimulationView.getTheSimulationView() );
		return slider;
	}

	private static JSlider setupInitialPopulationSlider()
	{
		JSlider slider = new JSlider();
		slider.setMinimum( 0 );
		slider.setMaximum( 100 );
		slider.setMajorTickSpacing( 25 );
		slider.setMinorTickSpacing( 5 );
		slider.setPaintLabels( true );
		slider.setPaintTicks( true );
		slider.setValue( 25 );
		initialPopulationSlider = slider;
		return slider;
	}
	
	public static Graphics2D getDisplayGraphics()
	{
		return ((Graphics2D)displayArea.getGraphics());
	}
	
	public static int getInitialPopulation()
	{
		return initialPopulationSlider.getValue();
	}
	
	public static int getSimulationSpeed()
	{
		return simulationSpeedSlider.getValue();
	}
	
	private static void setSimulationSpeed()
	{
		int delayMillis = Math.abs( simulationSpeedSlider.getValue() - MAXDELAYMILLIS );
		board.setSimulationDelay( delayMillis + 1 );
	}
	
	public static int getPatchesCountX()
	{
		return PATCHESCOUNTSIDE;
	}

	public static int getPatchesCountY()
	{
		return PATCHESCOUNTSIDE;
	}

	public static int getPatchPixelSize()
	{
		return PATCHPIXELSIDE;
	}

	public static JPanel getDisplayArea()
	{
		return displayArea;
	}

	public static void main( String[] args )
	{
		getTheSimulationView();
	}

	@Override
	public void stateChanged( ChangeEvent e )
	{
		JSlider source = (JSlider)e.getSource();
		if ( !source.getValueIsAdjusting() )
		{
			setSimulationSpeed();
		}
	}
}
