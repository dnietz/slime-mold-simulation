package com.daven.java4.slimemold;

import java.awt.Point;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class SimulationObjectMoveCandidate
{
	private EightPointCompass orientation;
	private Point location;
	
	public EightPointCompass getOrientation()
	{
		return orientation;
	}

	public Point getLocation()
	{
		return location;
	}

	@SuppressWarnings( "unused" )
	private SimulationObjectMoveCandidate() {}
	
	public SimulationObjectMoveCandidate( EightPointCompass orientation, Point location )
	{
		this.orientation = orientation;
		this.location = location;
	}
}
