package com.daven.java4.slimemold;

import java.awt.Point;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class SlimeMoldDrawPoints
{
	private Point tip;
	private Point leftEdge;
	private Point rightEdge;
	private Point shaftEnd;
	private static int drawSize;
	private static int halfDrawSize;
	private static boolean staticsSet = false;
	
	public Point getTip()
	{
		return tip;
	}
	public Point getLeftEdge()
	{
		return leftEdge;
	}
	public Point getRightEdge()
	{
		return rightEdge;
	}
	public Point getShaftEnd()
	{
		return shaftEnd;
	}
	
	private SlimeMoldDrawPoints() {}
	
	public static SlimeMoldDrawPoints getSlimeMoldDrawPoints( Point position, EightPointCompass orientation, int drawSize, int halfDrawSize )
	{
		setStatics( drawSize, halfDrawSize );
		SlimeMoldDrawPoints dp = new SlimeMoldDrawPoints();
		switch ( orientation )
		{
			case NORTHEAST:
			{
				dp.tip = getTopRight( position );
				dp.leftEdge = getTopCenter( position );
				dp.rightEdge = getMidRight( position );
				dp.shaftEnd = getBottomLeft( position );
				break;
			}
			case EAST:
			{
				dp.tip = getMidRight( position );
				dp.leftEdge = getTopCenter( position );
				dp.rightEdge = getBottomCenter( position );
				dp.shaftEnd = getMidLeft( position );
				break;
			}
			case SOUTHEAST:
			{
				dp.tip = getBottomRight( position );
				dp.leftEdge = getMidRight( position );
				dp.rightEdge = getBottomCenter( position );
				dp.shaftEnd = getTopLeft( position );
				break;
			}
			case SOUTH:
			{
				dp.tip = getBottomCenter( position );
				dp.leftEdge = getMidRight( position );
				dp.rightEdge = getMidLeft( position );
				dp.shaftEnd = getTopCenter( position );
				break;
			}
			case SOUTHWEST:
			{
				dp.tip = getBottomLeft( position );
				dp.leftEdge = getBottomCenter( position );
				dp.rightEdge = getMidLeft( position );
				dp.shaftEnd = getTopRight( position );
				break;
			}
			case WEST:
			{
				dp.tip = getMidLeft( position );
				dp.leftEdge = getBottomCenter( position );
				dp.rightEdge = getTopCenter( position );
				dp.shaftEnd = getMidRight( position );
				break;
			}
			case NORTHWEST:
			{
				dp.tip = getTopLeft( position );
				dp.leftEdge = getMidLeft( position );
				dp.rightEdge = getTopCenter( position );
				dp.shaftEnd = getBottomRight( position );
				break;
			}
			case NORTH: default:
			{
				dp.tip = getTopCenter( position );
				dp.leftEdge = getMidLeft( position );
				dp.rightEdge = getMidRight( position );
				dp.shaftEnd = getBottomCenter( position );
				break;
			}
		}
		return dp;
	}
	
	private static void setStatics( int drawSize, int halfDrawSize )
	{
		if ( !staticsSet )
		{
			SlimeMoldDrawPoints.drawSize = drawSize;
			SlimeMoldDrawPoints.halfDrawSize = halfDrawSize;
			staticsSet = true;
		}
	}
	
	private static Point getTopLeft( Point position )
	{
		return new Point( position );
	}
	
	private static Point getTopCenter( Point position )
	{
		return new Point( position.x + halfDrawSize, position.y );
	}
	
	private static Point getTopRight( Point position )
	{
		return new Point( position.x + drawSize, position. y );
	}
	
	private static Point getMidRight( Point position )
	{
		return new Point( position.x + drawSize, position.y + halfDrawSize );
	}
	
	private static Point getBottomRight( Point position )
	{
		return new Point( position.x + drawSize, position.y + drawSize );
	}
	
	private static Point getBottomCenter( Point position )
	{
		return new Point( position.x + halfDrawSize, position.y + drawSize );
	}
	
	private static Point getBottomLeft( Point position )
	{
		return new Point( position.x, position.y + drawSize );
	}
	
	private static Point getMidLeft( Point position )
	{
		return new Point( position.x, position.y + halfDrawSize );
	}
}
