package com.daven.java4.slimemold;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.HashMap;
import java.util.Map;
import com.daven.modelviewcontroller.SimulationObject;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class Patch implements SimulationObject
{
	private static final int MAXSTINKLEVEL = 6;

	private static Graphics2D graphics;
	private static Color initialColor;
	private static int sizePixels;
	private static boolean staticsSet = false;
	private static Map<Integer, Color> colorMap = new HashMap<Integer, Color>(5);

	private Point location;
	private Color color;
	private int stinkLevel;
	
	@Override
	public Point getPosition()
	{
		return location;
	}

	@Override
	public EightPointCompass getOrientation()
	{
		return EightPointCompass.NORTH;
	}

	@Override
	public void move() {}

	@Override
	public void paint()
	{
		this.setColorByStink();
		graphics.setColor( this.color );
		graphics.fillRect( this.location.x, this.location.y, sizePixels, sizePixels );
		this.stinkLevel = this.stinkLevel - 1;
		if ( this.stinkLevel < 0 )
			this.stinkLevel = 0;
	}
	
	public void setStinkLevel( int stinkLevel )
	{
		int level = this.stinkLevel + stinkLevel;
		if ( level > MAXSTINKLEVEL )
			level = MAXSTINKLEVEL;
		if ( level < 0 )
			level = 0;
		this.stinkLevel = level;
	}
	
	public void setStinkLevelWithoutExceeding( int stinkLevel )
	{
		if ( this.stinkLevel < stinkLevel )
			this.stinkLevel = stinkLevel;
	}
	
	public int getStinkLevel()
	{
		return stinkLevel;
	}
	
	public Patch( Point location, Graphics2D graphics, Color initialColor, int sizePixels )
	{
		if ( !staticsSet )
		{
			Patch.graphics = graphics;
			Patch.initialColor = color;
			Patch.sizePixels = sizePixels;
			Patch.colorMap.put( 1, new Color( 127, 255, 0 ) );
			Patch.colorMap.put( 2, new Color( 118, 238, 0 ) );
			Patch.colorMap.put( 3, new Color( 102, 205, 0 ) );
			Patch.colorMap.put( 4, new Color( 154, 205, 50 ) );
			Patch.colorMap.put( 5, new Color( 69, 139, 0 ) );
			Patch.staticsSet = true;
		}
		this.location = location;
		this.color = initialColor;
	}

	private void setColorByStink()
	{
		if ( this.stinkLevel > 0 )
		{
			this.color = Patch.colorMap.get( stinkLevel );
		}
		else
		{
			this.color = Patch.initialColor;
		}
	}
}
