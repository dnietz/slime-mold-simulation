package com.daven.java4.slimemold;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import com.daven.modelviewcontroller.ModelManager;
import com.daven.modelviewcontroller.SimulationObject;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class SlimeMoldCell implements SimulationObject
{
	private static final int MOVEMENTRATE = 1;
	private static final int STINKLEVEL = 3;

	private static ModelManager board;
	private static Color cellColor;
	private static Graphics2D g2;
	private static int drawSize;
	private static int halfDrawSize;
	private static boolean staticsSet = false;
	private static Random rand;

	private Point position;
	private Point drawPosition;
	private EightPointCompass orientation;
	
	@Override
	public Point getPosition()
	{
		return position;
	}

	@Override
	public EightPointCompass getOrientation()
	{
		return orientation;
	}
	
	@Override
	public void paint()
	{
		g2.setColor( cellColor );
		SlimeMoldDrawPoints drawPoints = resetDrawPoints();
		g2.drawLine( drawPoints.getTip().x, drawPoints.getTip().y, drawPoints.getLeftEdge().x, drawPoints.getLeftEdge().y );
		g2.drawLine( drawPoints.getTip().x, drawPoints.getTip().y, drawPoints.getRightEdge().x, drawPoints.getRightEdge().y );
		g2.drawLine( drawPoints.getTip().x, drawPoints.getTip().y, drawPoints.getShaftEnd().x, drawPoints.getShaftEnd().y );
	}
	
	@Override
	public void move()
	{
		board.releaseStink( this.position, STINKLEVEL );
		List<SimulationObjectMoveCandidate> moveCandidates = new ArrayList<SimulationObjectMoveCandidate>(3);
		moveCandidates.add( new SimulationObjectMoveCandidate( this.orientation, board.nextPatch( this.position, this.orientation, MOVEMENTRATE ) ) );
		moveCandidates.add( new SimulationObjectMoveCandidate( this.orientation.turnLeft(), board.nextPatch( this.position, orientation.turnLeft(), MOVEMENTRATE ) ) );
		moveCandidates.add( new SimulationObjectMoveCandidate( this.orientation.turnRight(), board.nextPatch( this.position, orientation.turnRight(), MOVEMENTRATE ) ) );
		List<SimulationObjectMoveCandidate> unoccupiedCandidates = new ArrayList<SimulationObjectMoveCandidate>(3);
		for ( SimulationObjectMoveCandidate c : moveCandidates )
		{
			if ( !board.isOccupied( c.getLocation() ) )
				unoccupiedCandidates.add( c );
		}
		List<SimulationObjectMoveCandidate> smellyCandidates = new ArrayList<SimulationObjectMoveCandidate>( unoccupiedCandidates.size() );
		int greatestStink = 0;
		for ( SimulationObjectMoveCandidate c : unoccupiedCandidates )
		{
			int thisStink = board.getStinkLevel( c.getLocation() );
			if ( thisStink > greatestStink )
			{
				smellyCandidates.clear();
				greatestStink = thisStink;
				smellyCandidates.add( c );
			}
			else if ( thisStink == greatestStink )
			{
				smellyCandidates.add( c );
			}
		}
		if ( smellyCandidates.size() > 0 )
		{
			int randomMoveChoice = rand.nextInt( smellyCandidates.size() );
			SimulationObjectMoveCandidate choice = smellyCandidates.get( randomMoveChoice );
			this.position = choice.getLocation();
			this.orientation = choice.getOrientation();
			updateDrawPosition();
		}
	}

	public SlimeMoldCell( ModelManager board, Point position, EightPointCompass orientation, Graphics2D graphics, int size, Random rand )
	{
		if ( !staticsSet )
		{
			SlimeMoldCell.board = board;
			cellColor = Color.RED;
			g2 = graphics;
			drawSize = size - 2;
			halfDrawSize = drawSize / 2;
			SlimeMoldCell.rand = rand;
			staticsSet = true;
		}
		this.position = new Point ( position.x, position.y );
		updateDrawPosition();
		this.orientation = orientation;
	}

	private void updateDrawPosition()
	{
		this.drawPosition = new Point ( this.position.x + 1, this.position.y + 1 );
	}
	
	private SlimeMoldDrawPoints resetDrawPoints()
	{
		return SlimeMoldDrawPoints.getSlimeMoldDrawPoints( drawPosition, orientation, drawSize, halfDrawSize );
	}
}
