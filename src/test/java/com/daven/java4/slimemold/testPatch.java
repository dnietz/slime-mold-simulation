package com.daven.java4.slimemold;

import static org.junit.Assert.*;
import java.awt.Color;
import java.awt.Point;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.daven.modelviewcontroller.SimulationView;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class testPatch
{
	@SuppressWarnings( "unused" )
	private static SimulationView view;
	private static Patch patch;
	private static final Color color = Color.BLUE;
	private static final Point position = new Point( 20, 20 );
	private static final int size = 10;
	
	@Before
	public void setUp() throws Exception
	{
		view = SimulationView.getTheSimulationView();
		patch = new Patch( position, SimulationView.getDisplayGraphics(), color, size );
	}

	@After
	public void tearDown() throws Exception
	{
		patch = null;
		view = null;
	}

	@Test
	public void testGetPosition()
	{
		assertSame( "getPosition() doesn't return the correct position", position, patch.getPosition() );
	}

	@Test
	public void testGetOrientation()
	{
		assertSame( "orientation not automatically set to North", EightPointCompass.NORTH, patch.getOrientation() );
	}

	@Test
	public void testMove()
	{
		patch.move();
		assertSame( "patch should not move", position, patch.getPosition() );
	}

	@Test
	public void testSetStinkLevelWithoutExceeding()
	{
		final int level = 2;
		patch.setStinkLevelWithoutExceeding( level + 1 );
		assertEquals( "setStinkLevelWithoutExceeding() didn't increase stink", level + 1, patch.getStinkLevel() );
		patch.setStinkLevelWithoutExceeding( level + 1 );
		assertEquals( "setStinkLevelWithoutExceeding() exceeded", level + 1, patch.getStinkLevel() );
		patch.setStinkLevelWithoutExceeding( level );
		assertEquals( "setStinkLevelWithoutExceeding() exceeded", level + 1, patch.getStinkLevel() );		
	}

	@Test
	public void testGetStinkLevel()
	{
		final int level = 2;
		final int twiceLevel = level + level;
		assertEquals( "initial stink value should be zero", 0, patch.getStinkLevel() );
		patch.setStinkLevel( level );
		assertEquals( "level should be where it was set", level, patch.getStinkLevel() );
		patch.setStinkLevel( level );
		assertEquals( "level should be additive", twiceLevel, patch.getStinkLevel() );
	}

	@Test
	public void testPatchConstructor()
	{
		assertTrue( "Patch() didn't return instance of Patch", Patch.class.isInstance( patch ) );
	}

}
