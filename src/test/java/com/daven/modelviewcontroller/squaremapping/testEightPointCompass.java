package com.daven.modelviewcontroller.squaremapping;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class testEightPointCompass
{
	private static final EightPointCompass N = EightPointCompass.NORTH;
	private static final EightPointCompass NE = EightPointCompass.NORTHEAST;
	private static final EightPointCompass E = EightPointCompass.EAST;
	private static final EightPointCompass SE = EightPointCompass.SOUTHEAST;
	private static final EightPointCompass S = EightPointCompass.SOUTH;
	private static final EightPointCompass SW = EightPointCompass.SOUTHWEST;
	private static final EightPointCompass W = EightPointCompass.WEST;
	private static final EightPointCompass NW = EightPointCompass.NORTHWEST;

	@Before
	public void setUp() throws Exception
	{
		
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testFromOrdinal()
	{
		assertSame( "-1 didn't return NW", NW, EightPointCompass.fromOrdinal( -1 ) );
		assertSame( "7 didn't return NW", NW, EightPointCompass.fromOrdinal( 7 ) );
		assertSame( "0 didn't return N", N, EightPointCompass.fromOrdinal( 0 ) );
		assertSame( "8 didn't return N", N, EightPointCompass.fromOrdinal( 8 ) );
	}

	@Test
	public void testTurnLeft()
	{
		assertSame( "NW should be left of N", NW, N.turnLeft() );
		assertSame( "N should be left of NE", N, NE.turnLeft() );
		assertSame( "NE should be left of E", NE, E.turnLeft() );
		assertSame( "E should be left of SE", E, SE.turnLeft() );
		assertSame( "SE should be left of S", SE, S.turnLeft() );
		assertSame( "S should be left of SW", S, SW.turnLeft() );
		assertSame( "SW should be left of W", SW, W.turnLeft() );
		assertSame( "W should be left of NW", W, NW.turnLeft() );
	}

	@Test
	public void testTurnRight()
	{
		assertSame( "NE should be right of N", NE, N.turnRight() );
		assertSame( "E should be right of NE", E, NE.turnRight() );
		assertSame( "SE should be right of E", SE, E.turnRight() );
		assertSame( "S should be right of SE", S, SE.turnRight() );
		assertSame( "SW should be right of S", SW, S.turnRight() );
		assertSame( "W should be right of SW", W, SW.turnRight() );
		assertSame( "NW should be right of W", NW, W.turnRight() );
		assertSame( "N should be right of NW", N, NW.turnRight() );
	}

}