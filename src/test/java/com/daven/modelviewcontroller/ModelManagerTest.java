package com.daven.modelviewcontroller;

import static org.junit.Assert.*;
import java.awt.Point;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.daven.modelviewcontroller.squaremapping.EightPointCompass;

public class ModelManagerTest
{
	private static final int populationCount = 25;
	
	@SuppressWarnings( "unused" )
	private static SimulationView view;
	private static ModelManager board;

	@Before
	public void setUp() throws Exception
	{
		try
		{
			view = SimulationView.getTheSimulationView();
			board = new ModelManager();
		}
		catch ( Exception ex )
		{
			fail( "could not instantiate SimulationView and/or ModelManager " + ex.getMessage() );
		}
	}

	@After
	public void tearDown() throws Exception
	{
		view = null;
		board = null;
	}

	@Test
	public void testModelManager()
	{
		assertTrue( "ModelManager constructor didn't return a ModelManager", ModelManager.class.isInstance( board ) );
	}

	@Test
	public void testMoveOneRound()
	{
		try
		{
			testRunInitialSetup();
		}
		catch ( Exception ex )
		{
			fail( "moveOneRound threw an exception" );
		}
	}

	@Test
	public void testMoveAllObjects()
	{
		try
		{
			board.moveAllObjects();
		}
		catch ( Exception ex )
		{
			fail( "moveAllObjects threw an exception" );
		}
	}

	@Test
	public void testModelChanged()
	{
		try
		{
			board.modelChanged();
		}
		catch ( Exception ex )
		{
			fail( "modelChanged threw an exception" );
		}
	}

	public void testReleaseStink( Point p, int stink )
	{
		testRunInitialSetup();
		try
		{
			board.releaseStink( p, stink );
		}
		catch ( Exception ex )
		{
			fail( "releaseStink threw an exception " + ex.getMessage() );
		}
	}

	@Test
	public void testGetStinkLevel()
	{
		final Point P = new Point( 0, 0 );
		int stink = 5;
		testReleaseStink( P, stink );
		assertEquals( "stink not set at point", 5, board.getStinkLevel( P ) );
	}

	@Test
	public void testNextPatch()
	{
		final Point P = new Point( 0, 0 );
		final Point LOWERRIGHTCORNER = new Point( 490, 490 );
		testRunInitialSetup();
		final Point NEXTPATCH = board.nextPatch( P, EightPointCompass.NORTHWEST, 1 );
		assertEquals( "didn't locate lower right corner patch", LOWERRIGHTCORNER, NEXTPATCH );
	}

	@Test
	public void testRunInitialSetup()
	{
		board.runInitialSetup();
		assertEquals( "population not set to " + populationCount, populationCount, SimulationView.getInitialPopulation() );
	}

	public void testSetSimulationDelay( int delay )
	{
		board.setSimulationDelay( delay );
	}

	@Test
	public void testGetSimulationDelay()
	{
		setAndTestSimulationDelay( 0 );
		setAndTestSimulationDelay( 5000 );
	}
	
	private void setAndTestSimulationDelay( int delay )
	{
		testSetSimulationDelay( delay );
		assertEquals( "delay not set to " + delay, delay, board.getSimulationDelay() );
	}

}
