package com.daven.modelviewcontroller;

import static org.junit.Assert.*;
import java.awt.Graphics2D;
import javax.swing.JPanel;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class SimulationViewTest
{
	private static final int INITIALPOPULATION = 25;
	private static final int INITIALSPEED = 501;
	private static final int PATCHESXORY = 50;
	private static final int PATCHSIZE = 10;
	
	private SimulationView view;
	
	@Before
	public void setUp() throws Exception
	{
		this.view = SimulationView.getTheSimulationView();
	}

	@After
	public void tearDown() throws Exception
	{
	}

	@Test
	public void testViewIsNotNullIsSimulationView()
	{
		assertNotNull( "Setup creates null view", view );
		assertEquals( "view not a SimulationView", SimulationView.class, view.getClass() );
	}
	
	@Test
	public void testGetTheSimulationView()
	{
		SimulationView v;
		try
		{
			assertNotNull( "SimulationView.getTheSimulationView() returns null",
					SimulationView.getTheSimulationView() );
			v = SimulationView.getTheSimulationView();
			assertSame( "view and v not pointing to same instance of SimulationView, must be singleton",
					view, v );
			assertEquals( "view and v have differing hashcodes", view.hashCode(), v.hashCode() );
		}
		catch ( Exception ex )
		{
			ex.printStackTrace();
		}
	}
	
	@Test
	public void testGetDisplayGraphics()
	{
		assertTrue( "Object return was not a Graphics2D", Graphics2D.class.isInstance( SimulationView.getDisplayGraphics() ) );
	}

	@Test
	public void testGetInitialPopulation()
	{
		assertTrue( "initial population !> 0", SimulationView.getInitialPopulation() > 0 );
		assertEquals( "initial population not " + INITIALPOPULATION, INITIALPOPULATION, SimulationView.getInitialPopulation() );
		try
		{
			@SuppressWarnings( "unused" )
			int i = SimulationView.getInitialPopulation();
		}
		catch ( ClassCastException ex )
		{
			fail( "casting getInitialPopulation() to int threw cast exception" );
		}
		catch ( Exception ex )
		{
			fail( ex.getMessage() );
		}
	}

	@Test
	public void testGetSimulationSpeed()
	{
		assertTrue( "simulation speed !> 0", SimulationView.getSimulationSpeed() > 0 );
		assertEquals( "simulation speed not " + INITIALSPEED, INITIALSPEED, SimulationView.getSimulationSpeed() );
		try
		{
			@SuppressWarnings( "unused" )
			int i = SimulationView.getSimulationSpeed();
		}
		catch ( ClassCastException ex )
		{
			fail( "casting getSimulationSpeed() to int threw cast exception" );
		}
		catch ( Exception ex )
		{
			fail( ex.getMessage() );
		}
	}

	@Test
	public void testGetPatchesCountX()
	{
		assertTrue( "patch count !> 0", SimulationView.getPatchesCountX() > 0 );
		assertEquals( "patch count not " + PATCHESXORY, PATCHESXORY, SimulationView.getPatchesCountX() );
		try
		{
			@SuppressWarnings( "unused" )
			int i = SimulationView.getPatchesCountX();
		}
		catch ( ClassCastException ex )
		{
			fail( "casting getPatchesCountX() to int threw cast exception" );
		}
		catch ( Exception ex )
		{
			fail( ex.getMessage() );
		}
	}

	@Test
	public void testGetPatchesCountY()
	{
		assertTrue( "patch count !> 0", SimulationView.getPatchesCountY() > 0 );
		assertEquals( "patch count not " + PATCHESXORY, PATCHESXORY, SimulationView.getPatchesCountY() );
		try
		{
			@SuppressWarnings( "unused" )
			int i = SimulationView.getPatchesCountY();
		}
		catch ( ClassCastException ex )
		{
			fail( "casting getPatchesCountY() to int threw cast exception" );
		}
		catch ( Exception ex )
		{
			fail( ex.getMessage() );
		}
	}

	@Test
	public void testGetPatchPixelSize()
	{
		assertTrue( "patch size !> 0", SimulationView.getPatchPixelSize() > 0 );
		assertEquals( "patch size not " + PATCHSIZE, PATCHSIZE, SimulationView.getPatchPixelSize() );
		try
		{
			@SuppressWarnings( "unused" )
			int i = SimulationView.getPatchPixelSize();
		}
		catch ( ClassCastException ex )
		{
			fail( "casting getPatchPixelSize() to int threw cast exception" );
		}
		catch ( Exception ex )
		{
			fail( ex.getMessage() );
		}
	}

	@Test
	public void testGetDisplayArea()
	{
		assertTrue( "object returned was not a JPanel", JPanel.class.isInstance( SimulationView.getDisplayArea() ) );
	}

}
